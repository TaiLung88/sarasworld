<?php

    require_once(__DIR__.'/../connessione.php');
    require_once(__DIR__.'/../../functions.php');

    $query = "CREATE TABLE utenti (
      id INT AUTO_INCREMENT PRIMARY KEY,
      nome VARCHAR(255) NOT NULL,
      cognome VARCHAR(255) NOT NULL,
      email VARCHAR(255) NOT NULL,
      userid VARCHAR(255) NOT NULL UNIQUE,
      password VARCHAR(255) NOT NULL
    )";

    mysqli_query($mysqli, $query);

    if ($error = sql_has_error($mysqli,$query))
    {

      echo $error;

    }else{
      echo "Tabella utenti creata!";
    }


 ?>
