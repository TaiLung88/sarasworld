<?php
    //richiedo la connessione al mio db
    require_once(__DIR__.'/db/connessione.php');
    //richiamo la pagina delle mie funzioni
    require_once(__DIR__.'/functions.php');

    session_start();
/*se i campi richiesti sono instanziati e 
  non nulli allora l'utente sta cercando di eseguire 
  una registrazione*/
    if (!empty($_POST['email'])
    && !empty($_POST['userid'])
    && !empty($_POST['nome'])
    && !empty($_POST['cognome']) 
    && !empty($_POST['password']))
    {
      //eseguo l'escape delle stringhe per ragioni di sicurezza
      $_POST['email'] =
      mysqli_real_escape_string($mysqli,$_POST['email']);
      
      $_POST['userid'] =
      mysqli_real_escape_string($mysqli,$_POST['userid']);

      $_POST['nome'] =
      mysqli_real_escape_string($mysqli,$_POST['nome']);

      $_POST['cognome'] =
      mysqli_real_escape_string($mysqli,$_POST['cognome']);

      $password =
      password_hash($_POST['password'], PASSWORD_BCRYPT);
      //preparo la mia query  
      $query = "INSERT INTO utenti (email, userid, password, nome, cognome) "
             . "VALUES ('{$_POST['email']}', '{$_POST['userid']}', '$password','{$_POST['nome']}', '{$_POST['cognome']}')";
    //la eseguo
      mysqli_query($mysqli, $query);
      /*verifico la presenza di errori avvalendomi della 
      mia funzione specificata in function.php*/
      if ($error = sql_has_error($mysqli, $query))
      {
        echo $error;
      }else{
        /*non ce ne sono */
        $query = "SELECT * FROM utenti
         WHERE userid = '{$_POST['userid']}'";

         echo "Utente registrato<br/>";
         
        $result = mysqli_query($mysqli, $query);
        $_SESSION['user'] = mysqli_fetch_assoc($result);
        header('location: areariservata.php');
      }
    }

    //richiedo testata del mio sito
    require_once 'testata.php';
        
 ?>
        <section id="medio">
            
            <section id="medioregistrazione">
                <h1 id="titolo1">REGISTRAZIONE UTENTE</h1>
                <form method="POST" >
                    <table id="meditabella" class="form">
                        <tr>
                            <td  class="mediotab">    
                                <h4>Nome:</h4>
                            </td>                        
                            <td class="mediotab">    
                            <input type="text" name="nome" placeholder="Nome" />
                            </td>        
                        </tr>
                        <tr>
                            <td  class="mediotab">    
                                <h4>Cognome:</h4>
                            </td>                        
                            <td class="mediotab">    
                                <input type="text" name="cognome" placeholder="Cognome" />
                            </td>        
                        </tr>  
                        <tr>
                            <td  class="mediotab">    
                                <h4>Email:</h4>
                            </td>                        
                            <td class="mediotab">    
                                <input type="email" name="email" placeholder="Email" />
                            </td>        
                        </tr>  
                        <tr>
                            <td  class="mediotab">    
                                <h4>Userid:</h4>
                            </td>                        
                            <td class="mediotab">    
                                <input type="username" name="userid" placeholder="Userid" />
                            </td>        
                        </tr>  
                        <tr>
                            <td  class="mediotab">    
                                <h4>Password:</h4>
                            </td>                        
                            <td class="mediotab">    
                                <input type="password" name="password" placeholder="Password" />
                            </td>        
                        </tr>  
                    </table>       
                    <input id="bottone" type="submit" value="Registrati" />
                </form>
           </section> 
            
        </section>
<?php
//richiedo il mio footer   
require_once 'footer.php';
