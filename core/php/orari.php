<?php
//richiedo testata del mio sito
require_once 'testata.php';

?>
        <section id="medio">

            <section id="mediorari">
                <h1 id="titolo1">ORARI NEGOZIO</h1>  
                <table id="meditabella">
                    <tr>
                        <td  class="mediotab">    
                            <h4>Lunedì</h4>
                        </td>                        
                        <td class="mediotab">    
                            9:00 - 12:30 | 15:00 - 17:30
                        </td>        
                    </tr>                    
                    <tr>
                        <td class="mediotab">    
                            <h4>Martedì</h4>
                        </td>                        
                        <td class="mediotab">    
                            9:00 - 12:30 | 15:00 - 17:30
                        </td>        
                    </tr>
                    <tr>
                        <td class="mediotab">    
                            <h4>Mercoledì</h4>
                        </td>                        
                        <td class="mediotab">    
                            9:00 - 12:30 | 15:00 - 17:30
                        </td>        
                    </tr>
                    <tr>
                        <td  class="mediotab">    
                            <h4>Giovedì</h4>
                        </td>                        
                        <td class="mediotab" id="chiuso">    
                            chiuso
                        </td>        
                    </tr>                    
                    <tr>
                        <td class="mediotab">    
                            <h4>Venerdì</h4>
                        </td>                        
                        <td class="mediotab">    
                            9:00 - 12:30 | 15:00 - 17:30
                        </td>        
                    </tr>
                    <tr>
                        <td class="mediotab">    
                            <h4>Sabato</h4>
                        </td>                        
                        <td class="mediotab">    
                            9:00 - 12:30 | 15:00 - 17:30
                        </td>        
                    </tr>
                </table>
            </section>
        </section>
       <?php
//richiedo il mio footer    
require_once 'footer.php';

?> 