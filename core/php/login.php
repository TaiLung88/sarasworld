<?php
  //richiedo la connessione al mio db
  require_once(__DIR__.'/db/connessione.php');
  //richiamo la pagina delle mie funzioni
  require_once(__DIR__.'/functions.php');

  session_start();
  /*se $_POST['userid'] e $_POST['password'] sono instanziati e 
  non nulli allora l'utente sta cercando di eseguire una login*/
  if (!empty($_POST['userid'])
      && !empty($_POST['password']))
    {
      //eseguo l'escape per ragioni di sicurezza
      $_POST['userid'] = mysqli_real_escape_string($mysqli,
      $_POST['userid']);
      //preparo la mia query
      $query = "SELECT * FROM utenti WHERE
       userid='{$_POST['userid']}'";
      //la eseguo ed attendo il risultato 
      $result = mysqli_query($mysqli, $query);
      /*verifico la presenza di errori avvalendomi della 
      mia funzione specificata in function.php*/
      if ($error = sql_has_error($mysqli,$query))
      {
        //lo stampo
        echo $error;
      }
      else
      {
        /*non ce ne sono quindi procedo con la verifica
        di validità della password*/
        $user = mysqli_fetch_assoc($result);

        if (password_verify($_POST['password'],
        $user['password']))
        {
          $_SESSION['user'] = $user;
          header('location: areariservata.php');
        }
        else
        {
          echo "Userid o password errati";
        }
       }
    }
    //richiedo testata del mio sito
    require_once 'testata.php';
        
 ?>
        <section id="medio">
            
          <section id="mediologin">
            <h1 id="titolo1">LOGIN UTENTE</h1>
            <form method="POST">
              <table id="meditabella" class="form">
                <tr>
                  <td  class="mediotab">    
                    <h4>Userid:</h4>
                  </td>                        
                  <td class="mediotab"> 
                    <input type="text" name="userid"/>       
                  </td>        
                </tr>
                <tr>
                  <td  class="mediotab">    
                    <h4>Password:</h4>
                  </td>                        
                  <td class="mediotab"> 
                    <input type="password" name="password"/>
                  </td>        
                </tr>  
              </table>      
              <input id="bottone" type="submit" value="Login">
            </form>
          </section>     
        </section>            
<?php
//richiedo il mio footer
require_once 'footer.php';
