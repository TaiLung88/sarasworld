<?php
//richiedo testata del mio sito    
require_once 'testata.php';
?>
<section id="medio">      
    <section id="medioarrivi">
         <div id="arrivi">
             <h1 id="titolo1">ULTIMI ARRIVI</h1>
             <div id="ar1">Fumetto</div>
             <div id="ar1a">
                 <img src="images/img1.jpg" alt=""/>
                 <p id="ar1ap">Creato da Tiziano Sclavi, Dylan Dog è il più celebre protagonista di una serie horror italiana, anche se "horror" è una definizione limitativa. Pubblicate a partire dal 1986 da Sergio Bonelli Editore.</p>
             </div>
             <div id="ar2">DVD</div>
             <div id="ar2a">
                 <img src="images/img2.jpg" alt=""/>
                 <p id="ar2ap">Lupin III è una serie di anime incentrati sul personaggio di Arsenio Lupin III, ideato nel 1967 dal mangaka Monkey Punch liberamente ispirato al personaggio di Arsène Lupin ideato da Maurice Leblanc.</p>
             </div>
             <div id="ar3">Gadget</div>
             <div id="ar3a">
                 <img src="images/img3.jpg" alt=""/>
                 <p id="ar3ap">Totoro è un personaggio creato nel 1988 da Hayao Miyazaki dello studio Ghibli, per il film Il mio vicino Totoro. Il suo aspetto è quello di un grosso animale calmo e bonario, che ama dormire e può rendersi invisibile a tutti. Sembra essere il custode e protettore della foresta.</p>
             </div>
         </div>
    </section>
</section>
<?php
//richiedo il mio footer    
require_once 'footer.php';