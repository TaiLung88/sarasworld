<!--questa è la mia testata del sito-->
<!DOCTYPE html>

<html>
    <head>
        <title>Arale Comics</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="js/jquery-3.2.1.min.js" type="text/javascript"></script>
        <script src="js/jquery.js" type="text/javascript"></script>
        <link href="css/reset.css" rel="stylesheet" type="text/css"/>
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
        <link href="https://fonts.googleapis.com/css?family=Quicksand:300,700" rel="stylesheet">
    </head>
    <body>
        <section id="omnia">
            <header id="alto">
                <img id="logo" src="images/logo.png" alt=""/>
            </header>
            <nav id="menu">
                <ul>
                    <li id="home"><a href="home.php">HOME</a></li>
                    <li id="ultimiarrivi"><a href="ultimiarrivi.php">ULTIMI ARRIVI</a></li>
                    <li id="login"><a href="login.php">LOGIN</a></li>
                    <li id="registrazione"><a href="registrazione.php">REGISTRAZIONE</a></li>
                    <li id="orari"><a href="orari.php">ORARI</a></li>
                    <li id="logout"><a href="logout.php">LOGOUT</a></li>
                </ul>
            </nav>
<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

