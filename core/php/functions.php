<?php

  /*
    $mysqli: connessione a mysql (il valore di ritorno
    di una mysqli_connect)

    return:
      false se tutto è andato bene
      errore: se c'è stato un errore


    Controlla se c'è stato un errore sull'ultima
    query eseguita sul database identificato da
    $mysqli
  */

  function sql_has_error($mysqli, $query)
  {

    if (mysqli_errno($mysqli))
    {
      $error = mysqli_error($mysqli);
      return "Errore durante la query: [$query] $error";

    }else {
      return false;
    }

  }

 ?>
